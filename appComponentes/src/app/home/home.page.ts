import { Component } from '@angular/core';

interface SerieProps {
  titulo: string;
  subtitulo?: string;
  capa: string;
  texto: string;
};

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  titulo = "Police Series";

  series: SerieProps[] = [
    {
      titulo: 'CSI: Miami',
      subtitulo: 'Crime Scene Investigation',
      capa: "https://flxt.tmsimg.com/assets/p184820_i_h10_ae.jpg",
      texto: "É uma série de televisão americana que mostrou o trabalho de investigação criminal de uma equipe em Miami. O seriado foi o primeiro spin-off e segunda série da franquia CSI: Crime Scene Investigation. O primeiro episódio foi exibido nos Estados Unidos em 9 de maio de 2002, como um episódio de CSI e foram produzidas 10 temporadas. A série foi produzida conjuntamente entre a Alliance Atlantis e a CBS Television Studios."
    },
    {
      titulo: "Brooklyn Nine-Nine",
      subtitulo: "Lei e Desordem",
      capa: "https://streamingsbrasil.com/wp-content/uploads/2022/01/Brooklyn-99-Temp-9-Thumbnail-1130x580.jpg",
      texto: "Brooklyn Nine-Nine (abreviado como B99) é uma série de televisão de comédia policial americana criada por Dan Goor e Michael Schur. A série gira em torno de Jake Peralta (Andy Samberg), um imaturo, mas talentoso, detetive da polícia de Nova York na fictícia 99.ª Delegacia do Brooklyn, que muitas vezes entra em conflito com seu novo comandante, o sério e severo capitão Raymond Holt (Andre Braugher)."
    },
    {
      titulo: "Criminal Minds",
      subtitulo: "Unidade de Análise Comportamental",
      capa: "https://multiversonoticias.com.br/wp-content/uploads/2022/02/multiverso-2.jpg",
      texto: "A equipe analisa criminosos do país por meio do modus operandi e a Vitimologia dos mesmos e antecipa seus próximos movimentos antes de eles agirem outra vez. Neste quesito, a série difere-se de outros dramas policias por focar mais no comportamento criminal do suspeito e elaboração de seu perfil (como profiler) do que o crime em si."
    },
    {
      titulo: "Chicago P.D",
      subtitulo: "Chicago P.D.: Distrito 21",
      capa: "https://tm.ibxk.com.br/2021/03/11/11173839556421.jpg?ims=1200x675",
      texto: "A Unidade de Inteligência da Polícia de Chicago, liderada pelo detetive sargento Hank Voight, combate o crime organizado, tráfico de drogas, assassinatos e muito mais."
    },
    {
      titulo: "Hawaii Five-0",
      subtitulo: "Hawaii: Força Especial",
      capa: "https://mixdeseries.com.br/wp-content/uploads/2021/11/Hawaii-Five-0-1-temporada.jpg",
      texto: "Uma força-tarefa especial montada pelo governador do estado do Havaí, que fica conhecida como 5-0, é a responsável por investigar crimes mais graves ocorridos na ilha. O Seal Steve McGarrett é o líder do time, que conta com o Detetive Danny Williams em segundo no comando, o ex-policial Chin Ho-Kelly e sua prima, a policial Kono Kalakaua"
    }
  ];

  constructor() {}

}
